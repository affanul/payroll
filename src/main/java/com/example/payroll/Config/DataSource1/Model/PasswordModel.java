package com.example.payroll.Config.DataSource1.Model;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class PasswordModel {

	private String USERNAME;
	@NotNull @NotEmpty(message = "Old Password is mandatory")
	private String OLDPASSWORD;
	@NotNull @NotEmpty(message = "New Password is mandatory")
	private String NEWPASSWORD;
	@NotNull @NotEmpty(message = "Confirm Password is mandatory")
	private String CONFPASSWORD;
	public String getUSERNAME() {
		return USERNAME;
	}
	public void setUSERNAME(String uSERNAME) {
		USERNAME = uSERNAME;
	}
	public String getOLDPASSWORD() {
		return OLDPASSWORD;
	}
	public void setOLDPASSWORD(String oLDPASSWORD) {
		OLDPASSWORD = oLDPASSWORD;
	}
	public String getNEWPASSWORD() {
		return NEWPASSWORD;
	}
	public void setNEWPASSWORD(String nEWPASSWORD) {
		NEWPASSWORD = nEWPASSWORD;
	}
	public String getCONFPASSWORD() {
		return CONFPASSWORD;
	}
	public void setCONFPASSWORD(String cONFPASSWORD) {
		CONFPASSWORD = cONFPASSWORD;
	}
	
	
}
