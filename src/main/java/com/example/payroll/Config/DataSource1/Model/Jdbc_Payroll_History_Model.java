package com.example.payroll.Config.DataSource1.Model;

import java.sql.Date;

import lombok.Data;

@Data
public class Jdbc_Payroll_History_Model {
	String ID;
	String NAME;
	String FILE_NAME;
	String DATE_MOVED;
}
