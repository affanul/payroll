package com.example.payroll.Config.DataSource1.Controller;

import java.security.Principal;

import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import com.example.payroll.Config.DataSource1.Dao.Jdbc_Payroll_History_Dao;
import com.example.payroll.Config.DataSource1.Model.FindHistory;
import com.example.payroll.Utils.DisplayPageUser;

@Controller
public class Jdbc_Payroll_History_Controller {
	public static Logger logger = LogManager.getLogger(Jdbc_Payroll_History_Controller.class.getName());

	@Autowired
	Jdbc_Payroll_History_Dao historyDao;

	@GetMapping(value = "/history/list")
	public ModelMap list(String fileName, String date, Principal principal, Pageable page) {
		ModelMap data = new ModelMap();
		new DisplayPageUser(page, principal, data);
		if (fileName == null) {
			fileName="";
		}
		if(date == null) {
			date =  null;
		}
		FindHistory findHistory = new FindHistory();
		findHistory.setFileName(fileName);
		findHistory.setDate(date);
		data.addAttribute("findHistory",  findHistory);
		data.addAttribute("data", historyDao.Listhistory(fileName,date, page));
		data.addAttribute("user", principal.getName());
		return data;
	}

	@PostMapping(value = "/history/list")
	public ModelMap list2(@Valid FindHistory findHistory, Principal principal, BindingResult bindingResult,
			Pageable page) {
		return list(findHistory.getFileName(), findHistory.getDate(), principal, page);
	}
}
