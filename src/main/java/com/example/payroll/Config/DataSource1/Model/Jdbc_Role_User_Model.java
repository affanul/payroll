package com.example.payroll.Config.DataSource1.Model;

import java.util.Date;
public class Jdbc_Role_User_Model {
	

	private String ID;
	private String USERNAME;
	private String ROLE;
	private Date DATE_CREATED;
	private String USER_CREATED;
	public String getID() {
		return ID;
	}
	public void setID(String iD) {
		ID = iD;
	}
	public String getUSERNAME() {
		return USERNAME;
	}
	public void setUSERNAME(String uSERNAME) {
		USERNAME = uSERNAME;
	}
	public String getROLE() {
		return ROLE;
	}
	public void setROLE(String rOLE) {
		ROLE = rOLE;
	}
	public Date getDATE_CREATED() {
		return DATE_CREATED;
	}
	public void setDATE_CREATED(Date dATE_CREATED) {
		DATE_CREATED = dATE_CREATED;
	}
	public String getUSER_CREATED() {
		return USER_CREATED;
	}
	public void setUSER_CREATED(String uSER_CREATED) {
		USER_CREATED = uSER_CREATED;
	}
	
	


}
