package com.example.payroll.Config.DataSource1.Controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class LoginController {

	@GetMapping(value = "/login")
	public String loadForm(){
		System.out.println("form");
		return "loginpage";
	}
	
	@PostMapping(value = "/login")
	public String loadAwal(){
		System.out.println("awal");
		return "redirect:/bulksumit/list";
	}
	
	@GetMapping(value = "/")
	public String loadFirst() {
		return "redirect:/bulksumit/list";
	}
}