package com.example.payroll.Config.DataSource1.Rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.example.payroll.Config.DataSource1.Model.Jdbc_Role_Author_Model;

public class Jdbc_Role_Author_Rowmapper implements RowMapper<Jdbc_Role_Author_Model>{

	@Override
	public Jdbc_Role_Author_Model mapRow(ResultSet rs, int rowNum) throws SQLException {
		Jdbc_Role_Author_Model detail = new Jdbc_Role_Author_Model();
		detail.setUSERNAME(rs.getString("USERNAME"));
		detail.setROLEUSER(rs.getString("ROLEUSER"));
		detail.setROLE(rs.getString("ROLE"));
		return detail;
	}

}
