package com.example.payroll.Config.DataSource1.Rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.example.payroll.Config.DataSource1.Model.Jdbc_Payroll_History_Model;

public class Jdbc_Payroll_History_Rowmapper implements RowMapper<Jdbc_Payroll_History_Model> {

	@Override
	public Jdbc_Payroll_History_Model mapRow(ResultSet rs, int rowNum) throws SQLException {
		Jdbc_Payroll_History_Model detail = new Jdbc_Payroll_History_Model();

		detail.setID(rs.getString("ID"));
		detail.setNAME(rs.getString("NAME"));
		detail.setFILE_NAME(rs.getString("FILE_NAME"));
		detail.setDATE_MOVED(rs.getString("DATE_MOVED"));
//		detail.setDATE_MOVED(rs.getDate("DATE_MOVED"));
		
		return detail;
	}

}
