package com.example.payroll.Config.DataSource1.Model;

public class Jdbc_Role_Author_Model {
	
	private String USERNAME;
	private String ROLEUSER;
	private String ROLE;
	public String getUSERNAME() {
		return USERNAME;
	}
	public void setUSERNAME(String uSERNAME) {
		USERNAME = uSERNAME;
	}
	public String getROLEUSER() {
		return ROLEUSER;
	}
	public void setROLEUSER(String rOLEUSER) {
		ROLEUSER = rOLEUSER;
	}
	public String getROLE() {
		return ROLE;
	}
	public void setROLE(String rOLE) {
		ROLE = rOLE;
	}
	
	
}
