package com.example.payroll.Config.DataSource1.Model;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;


public class Jdbc_User_Model {
	
	@NotNull @NotEmpty(message = "username is mandatory")
	private String username;
	@NotNull @NotEmpty(message = "password is mandatory")
	private String password;
	@NotNull @NotEmpty(message = "name is mandatory")
	private String name;
	private boolean enabled;
	@NotNull @NotEmpty(message = "group handling is mandatory")
	private String group_handling;
	private Date tgl_created;
	private String user_created;
	private boolean delete;
	private Date tgl_edited;
	private String user_edited;
	private List<String> role_user;
	private List<Jdbc_Role_User_Model> role_user_model;
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public boolean isEnabled() {
		return enabled;
	}
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	public String getGroup_handling() {
		return group_handling;
	}
	public void setGroup_handling(String group_handling) {
		this.group_handling = group_handling;
	}
	public Date getTgl_created() {
		return tgl_created;
	}
	public void setTgl_created(Date tgl_created) {
		this.tgl_created = tgl_created;
	}
	public String getUser_created() {
		return user_created;
	}
	public void setUser_created(String user_created) {
		this.user_created = user_created;
	}
	public boolean isDelete() {
		return delete;
	}
	public void setDelete(boolean delete) {
		this.delete = delete;
	}
	public Date getTgl_edited() {
		return tgl_edited;
	}
	public void setTgl_edited(Date tgl_edited) {
		this.tgl_edited = tgl_edited;
	}
	public String getUser_edited() {
		return user_edited;
	}
	public void setUser_edited(String user_edited) {
		this.user_edited = user_edited;
	}
	public List<String> getRole_user() {
		return role_user;
	}
	public void setRole_user(List<String> role_user) {
		this.role_user = role_user;
	}
	public List<Jdbc_Role_User_Model> getRole_user_model() {
		return role_user_model;
	}
	public void setRole_user_model(List<Jdbc_Role_User_Model> role_user_model) {
		this.role_user_model = role_user_model;
	}

}
