package com.example.payroll.Config.DataSource1.Controller;


import java.security.Principal;
import java.util.Date;

import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.example.payroll.Config.DataSource1.Dao.Jdbc_Payroll_User_Dao;
import com.example.payroll.Config.DataSource1.Model.Jdbc_List_Role_Model;
import com.example.payroll.Config.DataSource1.Model.Jdbc_Payroll_User_Model;
import com.example.payroll.Config.DataSource1.Model.Jdbc_Role_User_Model;
import com.example.payroll.Config.DataSource1.Model.PasswordModel;
import com.example.payroll.Utils.dateFormate;



@Controller
public class UserController {
	
	public static Logger logger = LogManager.getLogger(UserController.class.getName());
	
	
	@Autowired 
	private Jdbc_Payroll_User_Dao jdbcUserDao;
	
	@Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;
	
	
	@ModelAttribute("listRole")
	public Iterable<Jdbc_List_Role_Model> listRole(){
		return jdbcUserDao.listRole();
	}

	@GetMapping(value = "/admin/addClient")
	public ModelMap add(Principal principal, Jdbc_Payroll_User_Model model,String errorinfo) {
		ModelMap data = new ModelMap();
		data.addAttribute("errorinfo", "Add User");
//		data.addAttribute("errorinfo", data);
		data.addAttribute("user", principal.getName());
		return data;
	}
	
	@PostMapping(value = "/admin/addClient")
	public ModelAndView addProcess(@Valid Jdbc_Payroll_User_Model model, BindingResult bindingResult,
			Principal principal, RedirectAttributes attributes) {
		ModelMap data = new ModelMap();
		try {
			if(bindingResult.hasErrors()) {
				System.out.println("cek point 1 ");
				data.addAttribute("status", "file failed to save");
				return new ModelAndView("redirect:/admin/addClient", data);
			}
			else {
				System.out.println("name "+	model.getNAME());
				System.out.println("pass "+	model.getPASSWORD());
				System.out.println("user "+	model.getUSERNAME());
				model.setPASSWORD(bCryptPasswordEncoder.encode(model.getPASSWORD()));
				String dateNow = dateFormate.tanggal_2();
				Date date = dateFormate.formatDate10(dateNow);
				java.sql.Date tgl1 = new java.sql.Date(date.getTime());
				model.setDATE_CREATED(tgl1);
				model.setUSER_CREATED(principal.getName());
				model.setISDELETE("0");
				jdbcUserDao.InsertClient(model);
				
				System.out.println("cek point 8 "+model.getROLE_USER());
				for (String role_user : model.getROLE_USER()) {					
					System.out.println("role user "+role_user);
					Jdbc_Role_User_Model roleModel = new Jdbc_Role_User_Model();
					roleModel.setID(dateFormate.generateId());
					roleModel.setROLE(role_user);
					roleModel.setUSERNAME(model.getUSERNAME());
					jdbcUserDao.addRole(roleModel);
				}
				System.out.println(model);
				data.addAttribute("status", "file save succesfully");
				return new ModelAndView("redirect:/admin/addClient");
			}
		}catch (Exception e) {
			data.addAttribute("errorinfo", e.toString());
			e.printStackTrace();
			return new ModelAndView("redirect:/admin/addClient", data);
		}
	}
	
	@GetMapping(value = "/admin/listClient")
	public ModelMap list(Jdbc_Payroll_User_Model model,Principal principal, Pageable page) {
		ModelMap data = new ModelMap();
		
		if(model.getUSERNAME() == null){
			model.setUSERNAME("");
		}
		String username = "%"+model.getUSERNAME()+"%";
		data.addAttribute("user", principal.getName());
		data.addAttribute("data", jdbcUserDao.list(username, page));
		return data;
	}
	
	@PostMapping(value = "/admin/listClient")
	public ModelMap list2(@Valid Jdbc_Payroll_User_Model model, Principal principal, BindingResult bindingResult, Pageable pageable) {
		
		return list(model, principal, pageable);
	}
	
	@GetMapping(value = "/admin/detailClient")
	public String detail(@RequestParam("username") String username, Model model, Principal principal) {
		model.addAttribute("user", principal.getName());
		model.addAttribute("data", jdbcUserDao.detailClient(username));
//		model.addAttribute("role", jdbcUserDao.detailRole(username));
		return "admin/detailClient";
	}
	
	@PostMapping (value = "/admin/deleteClient")
	public String delete(@RequestParam String USERNAME, Principal principal) {
		Jdbc_Payroll_User_Model user = new Jdbc_Payroll_User_Model();
		String dateNow = dateFormate.tanggal_2();
		Date date = dateFormate.formatDate10(dateNow);
		java.sql.Date tgl1 = new java.sql.Date(date.getTime());
		user.setUSERNAME(USERNAME);
		user.setISDELETE("1");
		user.setDATE_EDITED(tgl1);
		user.setUSER_EDITED(principal.getName());
		jdbcUserDao.delete(user);
		
		return "redirect:/admin/listClient";
	}
	
	@GetMapping(value = "/admin/editClient")
	public String edit(@RequestParam String username,Model model,Principal principal,String errorinfo) {
		
		if(errorinfo==null){
			errorinfo = "Edit Data";
		}
		Jdbc_Payroll_User_Model user_Model = jdbcUserDao.detailClient(username);
		user_Model.setROLE_USER_MODEL(jdbcUserDao.findUserRole(username));
		
		
		model.addAttribute("jdbc_Payroll_User_Model", user_Model);
		model.addAttribute("user", principal.getName());
		model.addAttribute("errorinfo", errorinfo);
		return "admin/editClient";
	}
	
	@PostMapping(value = "/admin/editClient")
	public ModelAndView editProccess(@Valid Jdbc_Payroll_User_Model payroll_User_Model,
			BindingResult bindingResult, Principal principal) {
		ModelMap data = new ModelMap();
		try {
			if (bindingResult.hasErrors()) {
				data.addAttribute("errorinfo", bindingResult.getAllErrors().get(0).toString());
				data.addAttribute("username", payroll_User_Model.getUSERNAME());
				return new ModelAndView("redirect:/admin/editClient", data);
			}
			
			else {
				System.out.println(payroll_User_Model.getUSERNAME());
				System.out.println(payroll_User_Model.getPASSWORD());
				Jdbc_Payroll_User_Model detail = jdbcUserDao.detailClient(payroll_User_Model.getUSERNAME());
				String dateNow = dateFormate.tanggal_2();
				Date date = dateFormate.formatDate10(dateNow);
				java.sql.Date tgl1 = new java.sql.Date(date.getTime());
				payroll_User_Model.setDATE_EDITED(tgl1);
				payroll_User_Model.setUSER_EDITED(principal.getName());
				payroll_User_Model.setPASSWORD(detail.getPASSWORD());
				logger.info(detail.getPASSWORD().toString());
				
				jdbcUserDao.editClient(payroll_User_Model);
				logger.info("Role "+payroll_User_Model.getROLE_USER());
				
			}
		}catch (Exception e) {
			data.addAttribute("errorinfo", e.toString());
			e.printStackTrace();
			
			return new ModelAndView("redirect:/admin/editClient="+payroll_User_Model.getUSERNAME(), data);
			
		}
		data.addAttribute("username", payroll_User_Model.getUSERNAME());
		return new ModelAndView("redirect:/admin/editClient", data);

	}
	@PostMapping(value = "/admin/editClient2")
	public ModelAndView editProccess2(@Valid Jdbc_Payroll_User_Model payroll_User_Model,
			BindingResult bindingResult, Principal principal) {
		ModelMap data = new ModelMap();
		try {
			if (bindingResult.hasErrors()) {
				data.addAttribute("errorinfo", bindingResult.getAllErrors().get(0).toString());
				data.addAttribute("username", payroll_User_Model.getUSERNAME());
				return new ModelAndView("redirect:/admin/editClient", data);
				
			}
			
			else {
			System.out.println(payroll_User_Model.getUSERNAME());
			System.out.println(payroll_User_Model.getPASSWORD());
			Jdbc_Payroll_User_Model detail = jdbcUserDao.detailClient(payroll_User_Model.getUSERNAME());
			String dateNow = dateFormate.tanggal_2();
			Date date = dateFormate.formatDate10(dateNow);
			java.sql.Date tgl1 = new java.sql.Date(date.getTime());
			payroll_User_Model.setDATE_EDITED(tgl1);
			payroll_User_Model.setUSER_EDITED(principal.getName());
			logger.info(detail.getPASSWORD().toString());
			
			jdbcUserDao.editClient(payroll_User_Model);
			logger.info("Role "+payroll_User_Model.getROLE_USER());
			jdbcUserDao.DeleteUserRole(payroll_User_Model.getUSERNAME());
			for (String role_user : payroll_User_Model.getROLE_USER()) {					
				System.out.println("role user "+role_user);
				
				Jdbc_Role_User_Model roleModel = new Jdbc_Role_User_Model();
				roleModel.setID(dateFormate.generateId());
				roleModel.setROLE(role_user);
				roleModel.setUSERNAME(payroll_User_Model.getUSERNAME());
				jdbcUserDao.addRole(roleModel);
			}
			}
		}catch (Exception e) {
			data.addAttribute("errorinfo", e.toString());
			e.printStackTrace();
			
			return new ModelAndView("redirect:/admin/editClient="+payroll_User_Model.getUSERNAME(), data);
			
		}
		data.addAttribute("username", payroll_User_Model.getUSERNAME());
		return new ModelAndView("redirect:/admin/editClient", data);
		
	}
	
	@GetMapping(value = "/adminuser/password")
	public ModelMap changepassword(Principal principal, PasswordModel passwordModel) {
		ModelMap data = new ModelMap();
		passwordModel.setUSERNAME(principal.getName());
		data.addAttribute("password", passwordModel);
		data.addAttribute("user", principal.getName());
		return data;
	}
	
	@PostMapping(value = "/adminuser/password")
	public  ModelMap changePasswordProccess (Principal principal, @Valid PasswordModel passwordModel,
			BindingResult bindingResult) {
		ModelMap data = new ModelMap();
		data.addAttribute("user", principal.getName());
		if(bindingResult.hasErrors()) {
			return data;
		}
		System.out.println(passwordModel.getOLDPASSWORD());
		System.out.println(passwordModel.getNEWPASSWORD());
		if(!passwordModel.getNEWPASSWORD().equals(passwordModel.getCONFPASSWORD())) {
			data.addAttribute("error", "new password and Confirm password must same");
			
		}else {
			Jdbc_Payroll_User_Model user_Model = jdbcUserDao.detailClient(passwordModel.getUSERNAME());
			if(bCryptPasswordEncoder.matches(passwordModel.getOLDPASSWORD(), user_Model.getPASSWORD())) {
				jdbcUserDao.changePassword(bCryptPasswordEncoder.encode(passwordModel.getNEWPASSWORD()), passwordModel.getUSERNAME());
				data.addAttribute("error", "Change Password Succesfully");
			}
		}
		return data;
	}
	
}