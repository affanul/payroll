package com.example.payroll.Config.DataSource1.Dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import javax.persistence.EntityManagerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ParameterizedPreparedStatementSetter;
import org.springframework.stereotype.Repository;

import com.example.payroll.Config.DataSource1.Model.Jdbc_Payroll_History_Model;
import com.example.payroll.Config.DataSource1.Rowmapper.Jdbc_Payroll_History_Rowmapper;
import com.example.payroll.Utils.dateFormate;

@Repository
public class Jdbc_Payroll_History_Dao {

	@Autowired
	@Qualifier("jdbcTemplate1")
	JdbcTemplate jdbcTemplate;

	@Autowired
	@Qualifier("entityManagerFactory1")
	private EntityManagerFactory entityManagerFactory;

	public int InsertHistoryMove(Jdbc_Payroll_History_Model e) {
		String query = "INSERT INTO PAYROLL_HISTORY(ID,NAME,FILE_NAME,DATE_MOVED) VALUES(sys_guid(),?,?,?)";
		int a = jdbcTemplate.update(query, new Object[] { e.getNAME(), e.getFILE_NAME(), e.getDATE_MOVED() });
		return a;
	}

	public int[][] batchInsertHistoryMove(List<Jdbc_Payroll_History_Model> ez, int batchSize) {

		int[][] insertCount = jdbcTemplate.batchUpdate(
				"INSERT INTO PAYROLL_HISTORY(ID,NAME,FILE_NAME,DATE_MOVED) VALUES(sys_guid(),?,?,?)", ez, batchSize,
				new ParameterizedPreparedStatementSetter<Jdbc_Payroll_History_Model>() {
					public void setValues(PreparedStatement ps, Jdbc_Payroll_History_Model e) throws SQLException {
						int idxColumn = 1;
						ps.setString(idxColumn++, e.getNAME());
						ps.setString(idxColumn++, e.getFILE_NAME());
						ps.setString(idxColumn++, e.getDATE_MOVED());
					}
				});

		return insertCount;
	}
	
	public Page<Jdbc_Payroll_History_Model> Listhistory(String name,String date, Pageable page) {
		String date1 = dateFormate.formatDate9(date);
		String count = "select count(ID) as jml from PAYROLL_HISTORY where FILE_NAME LIKE ? and DATE_MOVED LIKE ?";
		int total = jdbcTemplate.queryForObject(count, new Object[] { name, date }, (rs, rowNum) -> rs.getInt("jml"));

//		String query = "select * from PAYROLL_HISTORY where FILE_NAME LIKE ? AND ROWNUM <= " + page.getPageSize();
		String query = "Select  ID, NAME, FILE_NAME, DATE_MOVED "
				+ " from ( select  ID, NAME, FILE_NAME, DATE_MOVED, ROW_NUMBER() OVER (ORDER BY FILE_NAME) rowRank from PAYROLL_HISTORY " + 
				"WHERE  FILE_NAME=? or DATE_MOVED=? or (FILE_NAME=? and DATE_MOVED=?)) where rowRank BETWEEN "+page.getOffset()+" and "
				+ page.getOffset()+"+20";

		List<Jdbc_Payroll_History_Model> data = jdbcTemplate.query(query, new Object[] { name, date, name, date },
				new Jdbc_Payroll_History_Rowmapper());

		return new PageImpl<Jdbc_Payroll_History_Model>(data, page, total);
	}
	
	public int getFileProcessed(String file_name) {
		String query = "select count(*) as jml from PAYROLL_HISTORY where FILE_NAME=?";
		int total = jdbcTemplate.queryForObject(query, new Object[] { file_name }, (rs, rowNum) -> rs.getInt("jml"));
		return total;
	}
}
