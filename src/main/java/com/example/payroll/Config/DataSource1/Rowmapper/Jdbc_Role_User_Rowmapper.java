package com.example.payroll.Config.DataSource1.Rowmapper;


import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.example.payroll.Config.DataSource1.Model.Jdbc_Role_User_Model;


public class Jdbc_Role_User_Rowmapper implements RowMapper<Jdbc_Role_User_Model>{

	@Override
	public Jdbc_Role_User_Model mapRow(ResultSet rs, int rowNum) throws SQLException {
		Jdbc_Role_User_Model detail = new Jdbc_Role_User_Model();
		detail.setID(rs.getString("ID"));
		detail.setUSERNAME(rs.getString("USERNAME"));
		detail.setROLE(rs.getString("ROLE"));
		detail.setDATE_CREATED(rs.getDate("DATE_CREATED"));
		detail.setUSER_CREATED(rs.getString("USER_CREATED"));
		return detail;
	}

}
