package com.example.payroll.Config.Datasource2;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
        entityManagerFactoryRef = "entityManagerFactory2", 
        transactionManagerRef = "transactionManager2",
        basePackages = {"com.example.payroll.Config.DataSource2"}
        )
public class DtSource2 {
@Autowired Environment environment;
	
    @Bean(name = "dataSource2")
    @ConfigurationProperties(prefix="payroll.datasource.oracle2")
    public DataSource DataSource2() {
        return DataSourceBuilder.create().build();
    }
	
    @Bean(name = "entityManagerFactory2")
    public LocalContainerEntityManagerFactoryBean entityManagerFactory2(
            EntityManagerFactoryBuilder builder,
            @Qualifier("dataSource2") DataSource dataSource2) {
        return builder
                .dataSource(dataSource2)
                .packages("com.example.payroll")
                .persistenceUnit("*")
                .build();
    }
	
    @Bean(name = "transactionManager2")
    public PlatformTransactionManager transactionManager2(
            @Qualifier("entityManagerFactory2") EntityManagerFactory entityManagerFactory2) {
        return new JpaTransactionManager(entityManagerFactory2);
    }
	
	@Autowired
	@Bean(name = "jdbcTemplate2")
    public JdbcTemplate JdbcTemplate2(@Qualifier("dataSource2")DataSource datasource2) {
		JdbcTemplate jdbcTemplateTemp = new JdbcTemplate(datasource2);
		return jdbcTemplateTemp;
    }
}

