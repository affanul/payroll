package com.example.payroll.Config.Datasource2.Controller;

import java.security.Principal;

import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.payroll.Config.DataSource1.Controller.Jdbc_Payroll_History_Controller;
import com.example.payroll.Config.DataSource1.Dao.Jdbc_Payroll_History_Dao;
import com.example.payroll.Config.DataSource1.Dao.Jdbc_Payroll_Sysparam_Dao;
import com.example.payroll.Config.DataSource1.Model.FindBulk;
import com.example.payroll.Config.DataSource1.Model.Jdbc_Payroll_History_Model;
import com.example.payroll.Config.DataSource1.Model.Jdbc_Payroll_Sysparam_Model;
import com.example.payroll.Config.Datasource2.Dao.Jdbc_Payroll_Bulksumit_Dao;
import com.example.payroll.Config.Datasource2.Model.Jdbc_Payroll_Bulksumlist_Model;
import com.example.payroll.Tools.Method;
import com.example.payroll.Utils.Constans;
import com.example.payroll.Utils.DisplayPageUser;
import com.example.payroll.Utils.dateFormate;

@Controller
public class Jdbc_Payroll_Bulksumit_Controller {
	public static Logger logger = LogManager.getLogger(Jdbc_Payroll_History_Controller.class.getName());

	@Autowired
	Jdbc_Payroll_Bulksumit_Dao bulksumitDao;
	
	@Autowired
	Jdbc_Payroll_History_Dao historyDao;
	
	@Autowired
	Jdbc_Payroll_Sysparam_Dao sysparamDao;
	
	@Autowired
	Method method;
	
	
	public ModelMap list3(String date, String date2 ,Principal principal, Pageable page) {
		ModelMap data = new ModelMap();
		new DisplayPageUser(page, principal, data);
	
		FindBulk findBulk = new FindBulk();
		findBulk.setDate(date);
		findBulk.setDate2(date2);
		data.addAttribute("findBulk", findBulk);
		data.addAttribute("data2", bulksumitDao.getByDate(date, date2));
		data.addAttribute("user", principal.getName());
		return data;
	}
	
	@GetMapping(value = "/bulksumit/list")
	public ModelMap list(String date, String date2 ,Principal principal, Pageable page) {
		ModelMap data = new ModelMap();
		new DisplayPageUser(page, principal, data);
		
		FindBulk findBulk = new FindBulk();
		findBulk.setDate(date);
		findBulk.setDate2(date2);
		data.addAttribute("findBulk", findBulk);
//		data.addAttribute("data2", bulksumitDao.getByDate(date, date2));
		data.addAttribute("data2", bulksumitDao.getAll());
		data.addAttribute("user", principal.getName());
		return data;
	}
     
	 
	@PostMapping(value = "/bulksumit/list")
	public ModelMap list2(@Valid FindBulk findBulk, Principal principal, BindingResult bindingResult,
			Pageable page) {
		return list3( findBulk.getDate(),findBulk.getDate2(), principal, page);
	}
	
	@GetMapping(value = "/bulksumit/list/move")
	public String move(@RequestParam String FILE_NAME, @RequestParam String ID, Principal principal){
		Jdbc_Payroll_Sysparam_Model getIncoming = sysparamDao.getByID(Constans.INCOMING_PATH_ID);
		Jdbc_Payroll_Sysparam_Model getOutgoing = sysparamDao.getByID(Constans.OUTGOINGPATH_ID);
		Jdbc_Payroll_History_Model historyModel = new Jdbc_Payroll_History_Model();
		Jdbc_Payroll_Bulksumlist_Model bulksumitModel = new Jdbc_Payroll_Bulksumlist_Model();
		
		logger.info(ID);
		
		if(method.moveFile(getIncoming.getNAME(), FILE_NAME, getOutgoing.getNAME())!=0){
			historyModel.setNAME(principal.getName());
			historyModel.setFILE_NAME(FILE_NAME);
			historyModel.setDATE_MOVED(dateFormate.tanggal_2());
			historyDao.InsertHistoryMove(historyModel);
			
			bulksumitModel.setFILE_NAME(FILE_NAME);
			bulksumitDao.UpdateBulksumlist(bulksumitModel);
			
		}
		return "redirect:/bulksumit/list";
	}
	
	@GetMapping(value = "/bulksumit/detail")
	public String detail(@RequestParam String FILE_NAME, Model model, Principal principal) {
		model.addAttribute("data", bulksumitDao.DetailBulksumlist(FILE_NAME));
		model.addAttribute("user", principal.getName());
		return "bulksumit/detail";
	}
}
