package com.example.payroll.Tools;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.example.payroll.Config.DataSource1.Dao.Jdbc_Payroll_History_Dao;
import com.example.payroll.Config.DataSource1.Dao.Jdbc_Payroll_Sysparam_Dao;
import com.example.payroll.Config.DataSource1.Model.Jdbc_Payroll_History_Model;
import com.example.payroll.Config.DataSource1.Model.Jdbc_Payroll_Sysparam_Model;
import com.example.payroll.Config.Datasource2.Dao.Jdbc_Payroll_Bulksumit_Dao;
import com.example.payroll.Config.Datasource2.Model.Jdbc_Payroll_Bulksumlist_Model;
import com.example.payroll.Utils.Constans;
import com.example.payroll.Utils.dateFormate;

@Component
public class Method {

	public static Logger logger = LogManager.getLogger(Method.class.getName());

	@Autowired
	Environment env;

	@Autowired
	Jdbc_Payroll_Sysparam_Dao sysparamDao;

	@Autowired
	Jdbc_Payroll_History_Dao historyDao;

	@Autowired

	Jdbc_Payroll_Bulksumit_Dao bulksumitDao;

	public File[] getFileList(String dirPath, String ext) {
		File dir = new File(dirPath);
		List<String> fileExt = new ArrayList<>();
		File[] fileList;
		fileList = dir.listFiles(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				File fileCheck = new File(dir + "/" + name);
				if (fileCheck.isFile()) {
					return name.endsWith(((ext != null && !ext.equals("")) ? ("." + ext) : ""));
				}
				return false;
			}
		});
		return fileList;
	}

	public List<Integer> moveFiles(String incomingpath, String outgoingpath) {
		List<Integer> result = new ArrayList<Integer>();

		List<Jdbc_Payroll_History_Model> historyModels = new ArrayList<Jdbc_Payroll_History_Model>();
		Jdbc_Payroll_History_Model historyModel = new Jdbc_Payroll_History_Model();

		List<Jdbc_Payroll_Bulksumlist_Model> bulsumlistModels = new ArrayList<Jdbc_Payroll_Bulksumlist_Model>();
		Jdbc_Payroll_Bulksumlist_Model bulsumlistModel = new Jdbc_Payroll_Bulksumlist_Model();

		Jdbc_Payroll_Sysparam_Model MaxBulkmoved = sysparamDao.getByID(Constans.MAXBULKMOVED);
		File[] fileList = getFileList(incomingpath, "");
		int maxMoved = Integer.parseInt(MaxBulkmoved.getNAME());
		int idx = 0;
		for (File file : fileList) {
			try {
				if (idx < maxMoved) {
					if(moveFile(incomingpath, file.getName(), outgoingpath)!=0) {						
						idx = idx + 1;
						
						historyModel.setNAME(Constans.DEFAULT_USER_MOVED);
						historyModel.setFILE_NAME(file.getName());
						historyModel.setDATE_MOVED(dateFormate.tanggal_2());
						
						bulsumlistModel.setFILE_NAME(file.getName());
						
						historyModels.add(historyModel);
						historyModel = new Jdbc_Payroll_History_Model();
						
						bulsumlistModels.add(bulsumlistModel);
						bulsumlistModel = new Jdbc_Payroll_Bulksumlist_Model();
						logger.info("File moved successfully movefile");
					} else {
						logger.info("File has been moved ");
						idx=0;
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		logger.info("total " + idx);
		idx = 0;
		historyDao.batchInsertHistoryMove(historyModels, historyModels.size());
		bulksumitDao.UpdateBulk(bulsumlistModels, bulsumlistModels.size());
		historyModels = new ArrayList<Jdbc_Payroll_History_Model>();
		return result;
	}

//	public void saveHistory()
	public Integer moveFile(String incoming, String filename, String outgoing) {
		int idx = 0;
		File file = new File(incoming + "/" + filename);

		if (historyDao.getFileProcessed(filename) >= 1) {
			logger.info("File " + filename + " has been moved");
			idx=0;
		} else {
			if (file.renameTo(new File(outgoing +"/" + file.getName()))) {
				file.delete();
				idx = idx + 1;

			} else {
				logger.info("File not moved " + file.getName());
			}
		}
		return idx;
	}
}
