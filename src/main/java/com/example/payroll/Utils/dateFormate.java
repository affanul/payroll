package com.example.payroll.Utils;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.Random;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class dateFormate {
public static Logger logger = LogManager.getLogger(dateFormate.class.getName());
	
	public static String formatDate(Date tanggal) {
		String result ="";
		try{
			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd",Locale.US);
			result = df.format(tanggal);
		}catch(Exception e){
			logger.error("Error Date :"+e);
		}
		return result;
	}

	public static Date formatDate2(String tanggal) {
		Date result =null;
		try{
			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",Locale.US);
			Calendar cal = Calendar.getInstance();
			cal.setTime(df.parse(tanggal));
			result = cal.getTime();
		}catch(Exception e){
			logger.error("Error Date :"+e);
		}
		return result;
	}

	public static Date formatDate3(String tanggal) {
		Date result =null;
		try{
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy",Locale.US);
			Calendar cal = Calendar.getInstance();
			cal.setTime(df.parse(tanggal));
			result = cal.getTime();
		}catch(Exception e){
			logger.error("Error Date :"+e);
		}
		return result;
	}

	public static String formatDate4(Date tanggal) {
		String result ="";
		try{
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm");
			result = df.format(tanggal);
		}catch(Exception e){
			logger.error("Error formatDate4 :"+e);
		}
		return result;
	}

	public static String formatDate5(String tanggal) {
		Date result =null;
		String hasil = "";
		try{
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			Calendar cal = Calendar.getInstance();
			cal.setTime(df.parse(tanggal));
			result = cal.getTime();
			
			SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
			hasil = df2.format(result);
		}catch(Exception e){
			logger.error("Error formatDate5 "+e);
		}
		return hasil;
	}
	
	public static Date formatDate6(String tanggal) {
		Date result =null;
		try{
			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			Calendar cal = Calendar.getInstance();
			cal.setTime(df.parse(tanggal));
			result = cal.getTime();
		}catch(Exception e){
			logger.error("Error Date :"+e);
		}
		return result;
	}

	public static String formatDate7(String tanggal) {
		Date result =null;
		String hasil = "";
		try{
			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",Locale.US);
			Calendar cal = Calendar.getInstance();
			cal.setTime(df.parse(tanggal));
			result = cal.getTime();
			
			SimpleDateFormat df2 = new SimpleDateFormat("yyyyMMdd",Locale.US);
			hasil = df2.format(result);
		}catch(Exception e){
			logger.error("Error formatDate7 "+e);
		}
		return hasil;
	}
	
	public static String formatDate8(Date tanggal) {
		String result ="";
		try{
			SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
			result = df.format(tanggal);
		}catch(Exception e){
			logger.error("Error formatDate8 :"+e);
		}
		return result;
	}
	public static String formatDate9(String tanggal) {
		Date result =null;
		String hasil = "";
		try{
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			Calendar cal = Calendar.getInstance();
			cal.setTime(df.parse(tanggal));
			result = cal.getTime();
			
			SimpleDateFormat df2 = new SimpleDateFormat("dd-MMM-yy");
			hasil = df2.format(result).toUpperCase();
		}catch(Exception e){
			logger.error("Error formatDate5 "+e);
		}
		return hasil;
	}
	public static Date formatDate10(String tanggal) {
		Date result =null;
		Date result2 =null;
		String hasil = "";
		try{
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			Calendar cal = Calendar.getInstance();
			cal.setTime(df.parse(tanggal));
			result = cal.getTime();
			
			SimpleDateFormat df2 = new SimpleDateFormat("dd-MMM-yy");
			hasil = df2.format(result).toUpperCase();
			result2 = df2.parse(hasil);
			
		}catch(Exception e){
			logger.error("Error Date :"+e);
		}
		return result2;
	}
	
	public static Date tanggal_0() {
		Date result =null;
		try{
			Calendar cal = GregorianCalendar.getInstance();
			//SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",Locale.US);
			result = cal.getTime();
		}catch(Exception e){
			logger.error("Error Date :"+e);
		}
		return result;
	}

	public static String tanggal() {
		String result ="";
		try{
			Calendar cal = GregorianCalendar.getInstance();
			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",Locale.US);
			result = df.format(cal.getTime());
		}catch(Exception e){
			logger.error("Error Date :"+e);
		}
		return result;
	}
	
	public static String tanggal_1() {
		String result ="";
		try{
			Calendar cal = GregorianCalendar.getInstance();
			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd",Locale.US);
			result = df.format(cal.getTime());
		}catch(Exception e){
			logger.error("Error Date :"+e);
		}
		return result;
	}

	public static String tanggal_2() {
		String result ="";
		try{
			Calendar cal = GregorianCalendar.getInstance();
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy",Locale.US);
			result = df.format(cal.getTime());
		}catch(Exception e){
			logger.error("Error Date 2 :"+e);
		}
		return result;
	}
	
	public static String tanggal_3() {
		String result ="";
		try{
			Calendar cal = GregorianCalendar.getInstance();
			SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
			result = df.format(cal.getTime());
		}catch(Exception e){
			logger.error("Error tanggal_3 :"+e);
		}
		return result;
	}
	
	public static String tanggal_4() {
		String result ="";
		try{
			Calendar cal = GregorianCalendar.getInstance();
			SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
			result = df.format(cal.getTime());
		}catch(Exception e){
			logger.error("Error tanggal_4 :"+e);
		}
		return result;
	}
	
	public static String jam() {
		String result ="";
		try{
			Calendar cal = GregorianCalendar.getInstance();
			SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss",Locale.US);
			result = df.format(cal.getTime());
		}catch(Exception e){
			logger.error("Error Date :"+e);
		}
		return result;
	}

	public static String formatnumber(String nominal) {
		String nilai=null;
		double nom = Double.parseDouble(nominal);
		nilai = NumberFormat.getNumberInstance(Locale.ENGLISH).format(nom);
		return nilai;
	}
	
	public static String generateId() {
		String result ="";
		try{
			Random random = new Random();
			int acak = random.nextInt(20);
			Calendar cal = GregorianCalendar.getInstance();
			SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmssSSS");
			result = df.format(cal.getTime());
			result = result+Integer.toString(acak);
		}catch(Exception e){
			logger.error("Error generateId :"+e);
		}
		return result;
	}
}
